import "@/styles/globals.css";
import { Navigation } from "../src/layouts/Navigation";
// import { Footer } from "../src/layouts/Footer";
import { EPageType, getProps } from "../src/services/content/getProps";
import { SpeedInsights } from "@vercel/speed-insights/next";
import { Analytics } from "@vercel/analytics/react";
import dotenv from "dotenv";
dotenv.config();

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const pageProps = await getProps({ pageType: EPageType.Index });

  return (
    <html lang="en">
      <body>
        <Navigation {...pageProps?.Navigation} />
        {children}
        <SpeedInsights />
        <Analytics />
        {/* <Footer {...pageProps?.Footer} /> */}
      </body>
    </html>
  );
}

export const revalidate = 1;
export const fetchCache = "default-cache";
