import { gql } from "graphql-request";

export const f_OffresEmploi = gql`
  fragment OffresEmploi on OffresEmploi {
    id
    lieuDeLoffre
    resumerDeLoffre
    slug
    titreDeLoffre
    typeDoffre
    categorie {
      ... on CategorieOffresDemploi {
        id
        name
      }
    text {
      json
    }
  }
`;
