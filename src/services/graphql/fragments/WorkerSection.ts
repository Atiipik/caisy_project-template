import { gql } from "graphql-request";

export const f_WorkerSection = gql`
  fragment WorkerSection on WorkerSection {
    title {
      ...Title
    }
    imageDeFond {
      ...Asset
    }
    couleurDeFond
    titreSecondaire
    slider {
      ... on WorkerSlider {
        categorie
        titre
        sousTitre
        description
        texteBouton
        lienBouton {
          ... on Page {
            slug
          }
          ... on BlogArticle {
            slug
          }
        }
        image {
          ...Asset
        }
        id
      }
    }
    id
  }
`;
