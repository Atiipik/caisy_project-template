import { gql } from "graphql-request";

export const f_WorkerSlider = gql`
  fragment WorkerSlider on WorkerSlider {
    categorie
    titre
    sousTitre
    description
    texteBouton
    lienBouton {
      ... on Page {
        slug
      }
      ... on BlogArticle {
        slug
      }
    }
    image {
      ...Asset
    }
    id
  }
`;
