import { gql } from "graphql-request";

export const f_Title = gql`
  fragment Title on Title {
    PrimaryText
    SecondaryText
    FontPrimaryText
    FontSecondaryText
    textColorOne {
      couleur
    }
    textColorTwo {
      couleur
    }
    id
  }
`;
