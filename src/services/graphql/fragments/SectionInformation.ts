import { gql } from "graphql-request";

export const f_SectionInformation = gql`
  fragment SectionInformation on SectionInformation {
    texte
    title {
      ...Title
    }
    bouton
    lienDuBouton {
      ...NavigationEntry
    }
    sectionImage {
      ...Asset
    }
    id
  }
`;
