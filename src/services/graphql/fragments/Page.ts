import { gql } from "graphql-request";
import { f_BlogArticleGrid } from "./BlogArticleGrid";
import { f_ContactForm } from "./ContactForm";
import { f_Fulltext } from "./Fulltext";
import { f_FullwidthBlogTeaser } from "./FullwidthBlogTeaser";
// import { f_Headline } from "./Headline";
import { f_NewsletterSignup } from "./NewsletterSignup";
import { f_SectionInformation } from "./SectionInformation";
import { f_WorkerSection } from "./WorkerSection";
import { f_WorkerSlider } from "./WorkerSlider";
import { f_Title } from "./Title";
import { f_OffresEmploiGrid } from "./OffresEmploiGrid";

export const f_Page = gql`
  ${f_ContactForm}
  ${f_BlogArticleGrid}
  ${f_NewsletterSignup}
  ${f_FullwidthBlogTeaser}
  ${f_Fulltext}
  ${f_SectionInformation}
  ${f_WorkerSection}
  ${f_WorkerSlider}
  ${f_Title}
  ${f_OffresEmploiGrid}

  fragment Page on Page {
    components {
      __typename
      ...ContactForm
      ...BlogArticleGrid
      ...NewsletterSignup
      ...FullwidthBlogTeaser
      ...Fulltext
      ...SectionInformation
      ...WorkerSection
      ...WorkerSlider
      ...Title
      ...OffresEmploiGrid
    }
    id
    seo {
      title
      ogImage {
        id
        src
        description
      }
      keywords
      id
      description
    }
    slug
  }
`;
