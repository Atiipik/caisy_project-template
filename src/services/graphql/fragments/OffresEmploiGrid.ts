import { gql } from "graphql-request";

export const f_OffresEmploiGrid = gql`
  fragment OffresEmploiGrid on OffresEmploiGrid {
    id
    description
    offresDemploi {
      ... on OffresEmploi {
        id
        lieuDeLoffre
        resumerDeLoffre
        categorie {
          ... on CategorieOffresDemploi {
            id
            name
          }
        }
        slug
        titreDeLoffre
        typeDoffre
        text {
          json
        }
      }
    }
    image {
      ...Asset
    }
    title {
      ...Title
    }
  }
`;
