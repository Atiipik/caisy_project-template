import { DocumentNode } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  JSON: any;
};

export type IGenAsset = {
  __typename?: 'Asset';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  author?: Maybe<Scalars['String']>;
  blurHash?: Maybe<Scalars['String']>;
  copyright?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  dominantColor?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  keywords?: Maybe<Scalars['String']>;
  originType?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  src?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  width?: Maybe<Scalars['Int']>;
};

export type IGenAsset_Connection = {
  __typename?: 'Asset_Connection';
  edges?: Maybe<Array<Maybe<IGenAsset_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenAsset_ConnectionEdge = {
  __typename?: 'Asset_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenAsset>;
};

export type IGenAsset_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenAsset_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenAsset_Nested_Where>>>;
  author?: InputMaybe<IGenCaisyField_String_Where>;
  blurHash?: InputMaybe<IGenCaisyField_String_Where>;
  copyright?: InputMaybe<IGenCaisyField_String_Where>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  dominantColor?: InputMaybe<IGenCaisyField_Color_Where>;
  height?: InputMaybe<IGenCaisyField_Number_WhereInt>;
  keywords?: InputMaybe<IGenCaisyField_String_Where>;
  originType?: InputMaybe<IGenCaisyField_String_Where>;
  originalName?: InputMaybe<IGenCaisyField_String_Where>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
  width?: InputMaybe<IGenCaisyField_Number_WhereInt>;
};

export type IGenAsset_Sort = {
  author?: InputMaybe<IGenOrder>;
  blurHash?: InputMaybe<IGenOrder>;
  copyright?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  description?: InputMaybe<IGenOrder>;
  dominantColor?: InputMaybe<IGenOrder>;
  height?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  keywords?: InputMaybe<IGenOrder>;
  originType?: InputMaybe<IGenOrder>;
  originalName?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
  width?: InputMaybe<IGenOrder>;
};

export type IGenAsset_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenAsset_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenAsset_Where>>>;
  author?: InputMaybe<IGenCaisyField_String_Where>;
  blurHash?: InputMaybe<IGenCaisyField_String_Where>;
  copyright?: InputMaybe<IGenCaisyField_String_Where>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  dominantColor?: InputMaybe<IGenCaisyField_Color_Where>;
  height?: InputMaybe<IGenCaisyField_Number_WhereInt>;
  keywords?: InputMaybe<IGenCaisyField_String_Where>;
  originType?: InputMaybe<IGenCaisyField_String_Where>;
  originalName?: InputMaybe<IGenCaisyField_String_Where>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
  width?: InputMaybe<IGenCaisyField_Number_WhereInt>;
};

export type IGenBlogArticle = {
  __typename?: 'BlogArticle';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  author?: Maybe<IGenBlogArticle_Author>;
  category?: Maybe<IGenCategory>;
  id?: Maybe<Scalars['ID']>;
  seo?: Maybe<IGenSeoInformation>;
  slug?: Maybe<Scalars['String']>;
  teaserDesciption?: Maybe<Scalars['String']>;
  teaserHeadline?: Maybe<Scalars['String']>;
  teaserImage?: Maybe<IGenAsset>;
  text?: Maybe<IGenBlogArticle_Text>;
};


export type IGenBlogArticleAuthorArgs = {
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenBlogArticleCategoryArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenBlogArticleSeoArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenBlogArticleTeaserImageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenBlogArticleTextArgs = {
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenBlogArticleGrid = {
  __typename?: 'BlogArticleGrid';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  articles?: Maybe<Array<Maybe<IGenBlogArticleGrid_Articles>>>;
  headline?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  subheadline?: Maybe<Scalars['String']>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenBlogArticleGridArticlesArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenBlogArticleGrid_Articles_Where = {
  findOne?: InputMaybe<IGenBlogArticleGrid_Articles_WhereConnection>;
};

export type IGenBlogArticleGrid_Articles_WhereConnection = {
  BlogArticle?: InputMaybe<IGenBlogArticle_Nested_Where>;
};

export type IGenBlogArticleGrid_Connection = {
  __typename?: 'BlogArticleGrid_Connection';
  edges?: Maybe<Array<Maybe<IGenBlogArticleGrid_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenBlogArticleGrid_ConnectionEdge = {
  __typename?: 'BlogArticleGrid_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenBlogArticleGrid>;
};

export type IGenBlogArticleGrid_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Nested_Where>>>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  subheadline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenBlogArticleGrid_Sort = {
  articles?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  headline?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  subheadline?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenBlogArticleGrid_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Where>>>;
  articles?: InputMaybe<IGenBlogArticleGrid_Articles_Where>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  subheadline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenBlogArticleGrid_Articles = IGenBlogArticle;

export type IGenBlogArticle_Category_Where = {
  findOne?: InputMaybe<IGenBlogArticle_Category_WhereConnection>;
};

export type IGenBlogArticle_Category_WhereConnection = {
  Category?: InputMaybe<IGenCategory_Nested_Where>;
};

export type IGenBlogArticle_Connection = {
  __typename?: 'BlogArticle_Connection';
  edges?: Maybe<Array<Maybe<IGenBlogArticle_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenBlogArticle_ConnectionEdge = {
  __typename?: 'BlogArticle_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenBlogArticle>;
};

export type IGenBlogArticle_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Nested_Where>>>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  teaserDesciption?: InputMaybe<IGenCaisyField_String_Where>;
  teaserHeadline?: InputMaybe<IGenCaisyField_String_Where>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
};

export type IGenBlogArticle_Seo_Where = {
  findOne?: InputMaybe<IGenBlogArticle_Seo_WhereConnection>;
};

export type IGenBlogArticle_Seo_WhereConnection = {
  SeoInformation?: InputMaybe<IGenSeoInformation_Nested_Where>;
};

export type IGenBlogArticle_Sort = {
  author?: InputMaybe<IGenOrder>;
  category?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  seo?: InputMaybe<IGenOrder>;
  slug?: InputMaybe<IGenOrder>;
  teaserDesciption?: InputMaybe<IGenOrder>;
  teaserHeadline?: InputMaybe<IGenOrder>;
  teaserImage?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenBlogArticle_TeaserImage_Where = {
  findOne?: InputMaybe<IGenBlogArticle_TeaserImage_WhereConnection>;
};

export type IGenBlogArticle_TeaserImage_WhereConnection = {
  Asset?: InputMaybe<IGenAsset_Nested_Where>;
};

export type IGenBlogArticle_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Where>>>;
  category?: InputMaybe<IGenBlogArticle_Category_Where>;
  seo?: InputMaybe<IGenBlogArticle_Seo_Where>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  teaserDesciption?: InputMaybe<IGenCaisyField_String_Where>;
  teaserHeadline?: InputMaybe<IGenCaisyField_String_Where>;
  teaserImage?: InputMaybe<IGenBlogArticle_TeaserImage_Where>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
};

export type IGenBlogArticle_Author = IGenBlogArticle | IGenCategorieOffresDemploi | IGenCategory | IGenCouleur | IGenNavigation | IGenNavigationEntry | IGenOffresEmploi | IGenPage | IGenSeoInformation;

export type IGenBlogArticle_Text = {
  __typename?: 'BlogArticle_text';
  connections?: Maybe<Array<Maybe<IGenBlogArticle_Text_Connections>>>;
  json?: Maybe<Scalars['JSON']>;
};


export type IGenBlogArticle_TextConnectionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenBlogArticle_Text_Connections = IGenAsset;

export type IGenCaisyDocument_Meta = {
  __typename?: 'CaisyDocument_Meta';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  locale?: Maybe<Scalars['String']>;
  locales?: Maybe<Array<Maybe<Scalars['String']>>>;
  publishedAt?: Maybe<Scalars['DateTime']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type IGenCaisyField_Color_Where = {
  contains?: InputMaybe<Scalars['String']>;
  eq?: InputMaybe<Scalars['String']>;
  neq?: InputMaybe<Scalars['String']>;
};

export type IGenCaisyField_Number_WhereInt = {
  eq?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  neq?: InputMaybe<Scalars['Int']>;
};

export type IGenCaisyField_Richtext_Where = {
  contains?: InputMaybe<Scalars['String']>;
  eq?: InputMaybe<Scalars['String']>;
  neq?: InputMaybe<Scalars['String']>;
};

export type IGenCaisyField_String_Where = {
  contains?: InputMaybe<Scalars['String']>;
  eq?: InputMaybe<Scalars['String']>;
  neq?: InputMaybe<Scalars['String']>;
};

export type IGenCategorieOffresDemploi = {
  __typename?: 'CategorieOffresDemploi';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
};

export type IGenCategorieOffresDemploi_Connection = {
  __typename?: 'CategorieOffresDemploi_Connection';
  edges?: Maybe<Array<Maybe<IGenCategorieOffresDemploi_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenCategorieOffresDemploi_ConnectionEdge = {
  __typename?: 'CategorieOffresDemploi_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenCategorieOffresDemploi>;
};

export type IGenCategorieOffresDemploi_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Nested_Where>>>;
  name?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenCategorieOffresDemploi_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  name?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenCategorieOffresDemploi_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Where>>>;
  name?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenCategory = {
  __typename?: 'Category';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
};

export type IGenCategory_Connection = {
  __typename?: 'Category_Connection';
  edges?: Maybe<Array<Maybe<IGenCategory_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenCategory_ConnectionEdge = {
  __typename?: 'Category_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenCategory>;
};

export type IGenCategory_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCategory_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCategory_Nested_Where>>>;
  name?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenCategory_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  name?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenCategory_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCategory_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCategory_Where>>>;
  name?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenContactForm = {
  __typename?: 'ContactForm';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  headline?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  titleInternal?: Maybe<Scalars['String']>;
};

export type IGenContactForm_Connection = {
  __typename?: 'ContactForm_Connection';
  edges?: Maybe<Array<Maybe<IGenContactForm_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenContactForm_ConnectionEdge = {
  __typename?: 'ContactForm_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenContactForm>;
};

export type IGenContactForm_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenContactForm_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenContactForm_Nested_Where>>>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenContactForm_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  headline?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenContactForm_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenContactForm_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenContactForm_Where>>>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenCouleur = {
  __typename?: 'Couleur';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  couleur?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
};

export type IGenCouleur_Connection = {
  __typename?: 'Couleur_Connection';
  edges?: Maybe<Array<Maybe<IGenCouleur_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenCouleur_ConnectionEdge = {
  __typename?: 'Couleur_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenCouleur>;
};

export type IGenCouleur_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCouleur_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCouleur_Nested_Where>>>;
  couleur?: InputMaybe<IGenCaisyField_Color_Where>;
};

export type IGenCouleur_Sort = {
  couleur?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenCouleur_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCouleur_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCouleur_Where>>>;
  couleur?: InputMaybe<IGenCaisyField_Color_Where>;
};

export type IGenCouleurs = {
  __typename?: 'Couleurs';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  couleur?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
};

export type IGenCouleurs_Connection = {
  __typename?: 'Couleurs_Connection';
  edges?: Maybe<Array<Maybe<IGenCouleurs_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenCouleurs_ConnectionEdge = {
  __typename?: 'Couleurs_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenCouleurs>;
};

export type IGenCouleurs_Sort = {
  couleur?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenCouleurs_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenCouleurs_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenCouleurs_Where>>>;
  couleur?: InputMaybe<IGenCaisyField_Color_Where>;
};

export type IGenFulltext = {
  __typename?: 'Fulltext';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  text?: Maybe<IGenFulltext_Text>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenFulltextTextArgs = {
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenFulltext_Connection = {
  __typename?: 'Fulltext_Connection';
  edges?: Maybe<Array<Maybe<IGenFulltext_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenFulltext_ConnectionEdge = {
  __typename?: 'Fulltext_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenFulltext>;
};

export type IGenFulltext_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenFulltext_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenFulltext_Nested_Where>>>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenFulltext_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenFulltext_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenFulltext_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenFulltext_Where>>>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenFulltext_Text = {
  __typename?: 'Fulltext_text';
  connections?: Maybe<Array<Maybe<IGenFulltext_Text_Connections>>>;
  json?: Maybe<Scalars['JSON']>;
};


export type IGenFulltext_TextConnectionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenFulltext_Text_Connections = IGenAsset;

export type IGenFullwidthBlogTeaser = {
  __typename?: 'FullwidthBlogTeaser';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  featuredArticle?: Maybe<IGenBlogArticle>;
  id?: Maybe<Scalars['ID']>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenFullwidthBlogTeaserFeaturedArticleArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenFullwidthBlogTeaser_Connection = {
  __typename?: 'FullwidthBlogTeaser_Connection';
  edges?: Maybe<Array<Maybe<IGenFullwidthBlogTeaser_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenFullwidthBlogTeaser_ConnectionEdge = {
  __typename?: 'FullwidthBlogTeaser_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenFullwidthBlogTeaser>;
};

export type IGenFullwidthBlogTeaser_FeaturedArticle_Where = {
  findOne?: InputMaybe<IGenFullwidthBlogTeaser_FeaturedArticle_WhereConnection>;
};

export type IGenFullwidthBlogTeaser_FeaturedArticle_WhereConnection = {
  BlogArticle?: InputMaybe<IGenBlogArticle_Nested_Where>;
};

export type IGenFullwidthBlogTeaser_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Nested_Where>>>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenFullwidthBlogTeaser_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  featuredArticle?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenFullwidthBlogTeaser_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Where>>>;
  featuredArticle?: InputMaybe<IGenFullwidthBlogTeaser_FeaturedArticle_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenLogoSection = {
  __typename?: 'LogoSection';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  logoSlider?: Maybe<Array<Maybe<IGenLogoSection_LogoSlider>>>;
  texte?: Maybe<Scalars['String']>;
  title?: Maybe<Array<Maybe<IGenLogoSection_Title>>>;
};


export type IGenLogoSectionLogoSliderArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenLogoSectionTitleArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenLogoSection_Connection = {
  __typename?: 'LogoSection_Connection';
  edges?: Maybe<Array<Maybe<IGenLogoSection_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenLogoSection_ConnectionEdge = {
  __typename?: 'LogoSection_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenLogoSection>;
};

export type IGenLogoSection_LogoSlider_Where = {
  findOne?: InputMaybe<IGenLogoSection_LogoSlider_WhereConnection>;
};

export type IGenLogoSection_LogoSlider_WhereConnection = {
  LogoSlider?: InputMaybe<IGenLogoSlider_Nested_Where>;
};

export type IGenLogoSection_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenLogoSection_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenLogoSection_Nested_Where>>>;
  texte?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenLogoSection_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  logoSlider?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  texte?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenLogoSection_Title_Where = {
  findOne?: InputMaybe<IGenLogoSection_Title_WhereConnection>;
};

export type IGenLogoSection_Title_WhereConnection = {
  Title?: InputMaybe<IGenTitle_Nested_Where>;
};

export type IGenLogoSection_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenLogoSection_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenLogoSection_Where>>>;
  logoSlider?: InputMaybe<IGenLogoSection_LogoSlider_Where>;
  texte?: InputMaybe<IGenCaisyField_String_Where>;
  title?: InputMaybe<IGenLogoSection_Title_Where>;
};

export type IGenLogoSection_LogoSlider = IGenLogoSlider;

export type IGenLogoSection_Title = IGenTitle;

export type IGenLogoSlider = {
  __typename?: 'LogoSlider';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  imageLogo?: Maybe<IGenAsset>;
  texteLogo?: Maybe<Scalars['String']>;
};


export type IGenLogoSliderImageLogoArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenLogoSlider_Connection = {
  __typename?: 'LogoSlider_Connection';
  edges?: Maybe<Array<Maybe<IGenLogoSlider_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenLogoSlider_ConnectionEdge = {
  __typename?: 'LogoSlider_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenLogoSlider>;
};

export type IGenLogoSlider_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Nested_Where>>>;
  texteLogo?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenLogoSlider_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  imageLogo?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  texteLogo?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenLogoSlider_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Where>>>;
  texteLogo?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenNavigation = {
  __typename?: 'Navigation';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  entries?: Maybe<Array<Maybe<IGenNavigation_Entries>>>;
  homePage?: Maybe<IGenPage>;
  id?: Maybe<Scalars['ID']>;
  notFoundPage?: Maybe<IGenPage>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenNavigationEntriesArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenNavigationHomePageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenNavigationNotFoundPageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenNavigationEntry = {
  __typename?: 'NavigationEntry';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  connection?: Maybe<IGenPage>;
  id?: Maybe<Scalars['ID']>;
  title?: Maybe<Scalars['String']>;
};


export type IGenNavigationEntryConnectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenNavigationEntry_Connection = {
  __typename?: 'NavigationEntry_Connection';
  edges?: Maybe<Array<Maybe<IGenNavigationEntry_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenNavigationEntry_ConnectionEdge = {
  __typename?: 'NavigationEntry_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenNavigationEntry>;
};

export type IGenNavigationEntry_Connection_Where = {
  findOne?: InputMaybe<IGenNavigationEntry_Connection_WhereConnection>;
};

export type IGenNavigationEntry_Connection_WhereConnection = {
  Page?: InputMaybe<IGenPage_Nested_Where>;
};

export type IGenNavigationEntry_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Nested_Where>>>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenNavigationEntry_Sort = {
  connection?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenNavigationEntry_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Where>>>;
  connection?: InputMaybe<IGenNavigationEntry_Connection_Where>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenNavigation_Entries = IGenNavigationEntry;

export type IGenNewsletterSignup = {
  __typename?: 'NewsletterSignup';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  headline?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  subheadline?: Maybe<Scalars['String']>;
  titleInternal?: Maybe<Scalars['String']>;
};

export type IGenNewsletterSignup_Connection = {
  __typename?: 'NewsletterSignup_Connection';
  edges?: Maybe<Array<Maybe<IGenNewsletterSignup_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenNewsletterSignup_ConnectionEdge = {
  __typename?: 'NewsletterSignup_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenNewsletterSignup>;
};

export type IGenNewsletterSignup_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Nested_Where>>>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  subheadline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenNewsletterSignup_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  headline?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  subheadline?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenNewsletterSignup_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Where>>>;
  headline?: InputMaybe<IGenCaisyField_String_Where>;
  subheadline?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenOffresEmploi = {
  __typename?: 'OffresEmploi';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  categorie?: Maybe<Array<Maybe<IGenOffresEmploi_Categorie>>>;
  id?: Maybe<Scalars['ID']>;
  lieuDeLoffre?: Maybe<Scalars['String']>;
  resumerDeLoffre?: Maybe<Scalars['String']>;
  seo?: Maybe<IGenSeoInformation>;
  slug?: Maybe<Scalars['String']>;
  text?: Maybe<IGenOffresEmploi_Text>;
  titreDeLoffre?: Maybe<Scalars['String']>;
  typeDoffre?: Maybe<Scalars['String']>;
};


export type IGenOffresEmploiCategorieArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenOffresEmploiSeoArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenOffresEmploiTextArgs = {
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenOffresEmploiGrid = {
  __typename?: 'OffresEmploiGrid';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  image?: Maybe<IGenAsset>;
  offresDemploi?: Maybe<Array<Maybe<IGenOffresEmploiGrid_OffresDemploi>>>;
  title?: Maybe<IGenTitle>;
};


export type IGenOffresEmploiGridImageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenOffresEmploiGridOffresDemploiArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenOffresEmploiGridTitleArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenOffresEmploiGrid_Connection = {
  __typename?: 'OffresEmploiGrid_Connection';
  edges?: Maybe<Array<Maybe<IGenOffresEmploiGrid_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenOffresEmploiGrid_ConnectionEdge = {
  __typename?: 'OffresEmploiGrid_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenOffresEmploiGrid>;
};

export type IGenOffresEmploiGrid_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Nested_Where>>>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenOffresEmploiGrid_OffresDemploi_Where = {
  findOne?: InputMaybe<IGenOffresEmploiGrid_OffresDemploi_WhereConnection>;
};

export type IGenOffresEmploiGrid_OffresDemploi_WhereConnection = {
  OffresEmploi?: InputMaybe<IGenOffresEmploi_Nested_Where>;
};

export type IGenOffresEmploiGrid_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  description?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  image?: InputMaybe<IGenOrder>;
  offresDemploi?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenOffresEmploiGrid_Title_Where = {
  findOne?: InputMaybe<IGenOffresEmploiGrid_Title_WhereConnection>;
};

export type IGenOffresEmploiGrid_Title_WhereConnection = {
  Title?: InputMaybe<IGenTitle_Nested_Where>;
};

export type IGenOffresEmploiGrid_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Where>>>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  offresDemploi?: InputMaybe<IGenOffresEmploiGrid_OffresDemploi_Where>;
  title?: InputMaybe<IGenOffresEmploiGrid_Title_Where>;
};

export type IGenOffresEmploiGrid_OffresDemploi = IGenOffresEmploi;

export type IGenOffresEmploi_Categorie_Where = {
  findOne?: InputMaybe<IGenOffresEmploi_Categorie_WhereConnection>;
};

export type IGenOffresEmploi_Categorie_WhereConnection = {
  CategorieOffresDemploi?: InputMaybe<IGenCategorieOffresDemploi_Nested_Where>;
};

export type IGenOffresEmploi_Connection = {
  __typename?: 'OffresEmploi_Connection';
  edges?: Maybe<Array<Maybe<IGenOffresEmploi_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenOffresEmploi_ConnectionEdge = {
  __typename?: 'OffresEmploi_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenOffresEmploi>;
};

export type IGenOffresEmploi_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Nested_Where>>>;
  lieuDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  resumerDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
  titreDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  typeDoffre?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenOffresEmploi_Seo_Where = {
  findOne?: InputMaybe<IGenOffresEmploi_Seo_WhereConnection>;
};

export type IGenOffresEmploi_Seo_WhereConnection = {
  SeoInformation?: InputMaybe<IGenSeoInformation_Nested_Where>;
};

export type IGenOffresEmploi_Sort = {
  categorie?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  lieuDeLoffre?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  resumerDeLoffre?: InputMaybe<IGenOrder>;
  seo?: InputMaybe<IGenOrder>;
  slug?: InputMaybe<IGenOrder>;
  titreDeLoffre?: InputMaybe<IGenOrder>;
  typeDoffre?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenOffresEmploi_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Where>>>;
  categorie?: InputMaybe<IGenOffresEmploi_Categorie_Where>;
  lieuDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  resumerDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  seo?: InputMaybe<IGenOffresEmploi_Seo_Where>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  text?: InputMaybe<IGenCaisyField_Richtext_Where>;
  titreDeLoffre?: InputMaybe<IGenCaisyField_String_Where>;
  typeDoffre?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenOffresEmploi_Categorie = IGenCategorieOffresDemploi;

export type IGenOffresEmploi_Text = {
  __typename?: 'OffresEmploi_text';
  connections?: Maybe<Array<Maybe<IGenOffresEmploi_Text_Connections>>>;
  json?: Maybe<Scalars['JSON']>;
};


export type IGenOffresEmploi_TextConnectionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenOffresEmploi_Text_Connections = IGenAsset;

export enum IGenOrder {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type IGenPage = {
  __typename?: 'Page';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  components?: Maybe<Array<Maybe<IGenPage_Components>>>;
  id?: Maybe<Scalars['ID']>;
  seo?: Maybe<IGenSeoInformation>;
  slug?: Maybe<Scalars['String']>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenPageComponentsArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenPageSeoArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenPageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage?: Maybe<Scalars['Boolean']>;
  hasPreviousPage?: Maybe<Scalars['Boolean']>;
  startCursor?: Maybe<Scalars['String']>;
};

export type IGenPage_Components_Where = {
  findOne?: InputMaybe<IGenPage_Components_WhereConnection>;
};

export type IGenPage_Components_WhereConnection = {
  BlogArticleGrid?: InputMaybe<IGenBlogArticleGrid_Nested_Where>;
  ContactForm?: InputMaybe<IGenContactForm_Nested_Where>;
  Fulltext?: InputMaybe<IGenFulltext_Nested_Where>;
  FullwidthBlogTeaser?: InputMaybe<IGenFullwidthBlogTeaser_Nested_Where>;
  LogoSection?: InputMaybe<IGenLogoSection_Nested_Where>;
  LogoSlider?: InputMaybe<IGenLogoSlider_Nested_Where>;
  NewsletterSignup?: InputMaybe<IGenNewsletterSignup_Nested_Where>;
  OffresEmploiGrid?: InputMaybe<IGenOffresEmploiGrid_Nested_Where>;
  SectionInformation?: InputMaybe<IGenSectionInformation_Nested_Where>;
  Title?: InputMaybe<IGenTitle_Nested_Where>;
  WorkerSection?: InputMaybe<IGenWorkerSection_Nested_Where>;
  WorkerSlider?: InputMaybe<IGenWorkerSlider_Nested_Where>;
};

export type IGenPage_Connection = {
  __typename?: 'Page_Connection';
  edges?: Maybe<Array<Maybe<IGenPage_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenPage_ConnectionEdge = {
  __typename?: 'Page_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenPage>;
};

export type IGenPage_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenPage_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenPage_Nested_Where>>>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenPage_Seo_Where = {
  findOne?: InputMaybe<IGenPage_Seo_WhereConnection>;
};

export type IGenPage_Seo_WhereConnection = {
  SeoInformation?: InputMaybe<IGenSeoInformation_Nested_Where>;
};

export type IGenPage_Sort = {
  components?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  seo?: InputMaybe<IGenOrder>;
  slug?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenPage_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenPage_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenPage_Where>>>;
  components?: InputMaybe<IGenPage_Components_Where>;
  seo?: InputMaybe<IGenPage_Seo_Where>;
  slug?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenPage_Components = IGenBlogArticleGrid | IGenContactForm | IGenFulltext | IGenFullwidthBlogTeaser | IGenLogoSection | IGenLogoSlider | IGenNewsletterSignup | IGenOffresEmploiGrid | IGenSectionInformation | IGenTitle | IGenWorkerSection | IGenWorkerSlider;

export type IGenQuery = {
  __typename?: 'Query';
  Asset?: Maybe<IGenAsset>;
  BlogArticle?: Maybe<IGenBlogArticle>;
  BlogArticleGrid?: Maybe<IGenBlogArticleGrid>;
  CategorieOffresDemploi?: Maybe<IGenCategorieOffresDemploi>;
  Category?: Maybe<IGenCategory>;
  ContactForm?: Maybe<IGenContactForm>;
  Couleur?: Maybe<IGenCouleur>;
  Couleurs?: Maybe<IGenCouleurs>;
  Fulltext?: Maybe<IGenFulltext>;
  FullwidthBlogTeaser?: Maybe<IGenFullwidthBlogTeaser>;
  LogoSection?: Maybe<IGenLogoSection>;
  LogoSlider?: Maybe<IGenLogoSlider>;
  Navigation?: Maybe<IGenNavigation>;
  NavigationEntry?: Maybe<IGenNavigationEntry>;
  NewsletterSignup?: Maybe<IGenNewsletterSignup>;
  OffresEmploi?: Maybe<IGenOffresEmploi>;
  OffresEmploiGrid?: Maybe<IGenOffresEmploiGrid>;
  Page?: Maybe<IGenPage>;
  SectionInformation?: Maybe<IGenSectionInformation>;
  SeoInformation?: Maybe<IGenSeoInformation>;
  Title?: Maybe<IGenTitle>;
  WorkerSection?: Maybe<IGenWorkerSection>;
  WorkerSlider?: Maybe<IGenWorkerSlider>;
  allAsset?: Maybe<IGenAsset_Connection>;
  allBlogArticle?: Maybe<IGenBlogArticle_Connection>;
  allBlogArticleGrid?: Maybe<IGenBlogArticleGrid_Connection>;
  allCategorieOffresDemploi?: Maybe<IGenCategorieOffresDemploi_Connection>;
  allCategory?: Maybe<IGenCategory_Connection>;
  allContactForm?: Maybe<IGenContactForm_Connection>;
  allCouleur?: Maybe<IGenCouleur_Connection>;
  allCouleurs?: Maybe<IGenCouleurs_Connection>;
  allFulltext?: Maybe<IGenFulltext_Connection>;
  allFullwidthBlogTeaser?: Maybe<IGenFullwidthBlogTeaser_Connection>;
  allLogoSection?: Maybe<IGenLogoSection_Connection>;
  allLogoSlider?: Maybe<IGenLogoSlider_Connection>;
  allNavigationEntry?: Maybe<IGenNavigationEntry_Connection>;
  allNewsletterSignup?: Maybe<IGenNewsletterSignup_Connection>;
  allOffresEmploi?: Maybe<IGenOffresEmploi_Connection>;
  allOffresEmploiGrid?: Maybe<IGenOffresEmploiGrid_Connection>;
  allPage?: Maybe<IGenPage_Connection>;
  allSectionInformation?: Maybe<IGenSectionInformation_Connection>;
  allSeoInformation?: Maybe<IGenSeoInformation_Connection>;
  allTitle?: Maybe<IGenTitle_Connection>;
  allWorkerSection?: Maybe<IGenWorkerSection_Connection>;
  allWorkerSlider?: Maybe<IGenWorkerSlider_Connection>;
};


export type IGenQueryAssetArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryBlogArticleArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryBlogArticleGridArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryCategorieOffresDemploiArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryCategoryArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryContactFormArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryCouleurArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryCouleursArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryFulltextArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryFullwidthBlogTeaserArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryLogoSectionArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryLogoSliderArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryNavigationArgs = {
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryNavigationEntryArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryNewsletterSignupArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryOffresEmploiArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryOffresEmploiGridArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryPageArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQuerySectionInformationArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQuerySeoInformationArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryTitleArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryWorkerSectionArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryWorkerSliderArgs = {
  id: Scalars['ID'];
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenQueryAllAssetArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenAsset_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenAsset_Where>>>;
};


export type IGenQueryAllBlogArticleArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenBlogArticle_Where>>>;
};


export type IGenQueryAllBlogArticleGridArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenBlogArticleGrid_Where>>>;
};


export type IGenQueryAllCategorieOffresDemploiArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenCategorieOffresDemploi_Where>>>;
};


export type IGenQueryAllCategoryArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenCategory_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenCategory_Where>>>;
};


export type IGenQueryAllContactFormArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenContactForm_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenContactForm_Where>>>;
};


export type IGenQueryAllCouleurArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenCouleur_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenCouleur_Where>>>;
};


export type IGenQueryAllCouleursArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenCouleurs_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenCouleurs_Where>>>;
};


export type IGenQueryAllFulltextArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenFulltext_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenFulltext_Where>>>;
};


export type IGenQueryAllFullwidthBlogTeaserArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenFullwidthBlogTeaser_Where>>>;
};


export type IGenQueryAllLogoSectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenLogoSection_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenLogoSection_Where>>>;
};


export type IGenQueryAllLogoSliderArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenLogoSlider_Where>>>;
};


export type IGenQueryAllNavigationEntryArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenNavigationEntry_Where>>>;
};


export type IGenQueryAllNewsletterSignupArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenNewsletterSignup_Where>>>;
};


export type IGenQueryAllOffresEmploiArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenOffresEmploi_Where>>>;
};


export type IGenQueryAllOffresEmploiGridArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenOffresEmploiGrid_Where>>>;
};


export type IGenQueryAllPageArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenPage_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenPage_Where>>>;
};


export type IGenQueryAllSectionInformationArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Where>>>;
};


export type IGenQueryAllSeoInformationArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Where>>>;
};


export type IGenQueryAllTitleArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenTitle_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenTitle_Where>>>;
};


export type IGenQueryAllWorkerSectionArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Where>>>;
};


export type IGenQueryAllWorkerSliderArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Sort>>>;
  where?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Where>>>;
};

export type IGenSectionInformation = {
  __typename?: 'SectionInformation';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  bouton?: Maybe<Scalars['String']>;
  couleurDuBouton?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  lienDuBouton?: Maybe<IGenNavigationEntry>;
  sectionImage?: Maybe<IGenAsset>;
  texte?: Maybe<Scalars['String']>;
  title?: Maybe<IGenTitle>;
};


export type IGenSectionInformationLienDuBoutonArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenSectionInformationSectionImageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenSectionInformationTitleArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenSectionInformation_Connection = {
  __typename?: 'SectionInformation_Connection';
  edges?: Maybe<Array<Maybe<IGenSectionInformation_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenSectionInformation_ConnectionEdge = {
  __typename?: 'SectionInformation_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenSectionInformation>;
};

export type IGenSectionInformation_LienDuBouton_Where = {
  findOne?: InputMaybe<IGenSectionInformation_LienDuBouton_WhereConnection>;
};

export type IGenSectionInformation_LienDuBouton_WhereConnection = {
  NavigationEntry?: InputMaybe<IGenNavigationEntry_Nested_Where>;
};

export type IGenSectionInformation_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Nested_Where>>>;
  bouton?: InputMaybe<IGenCaisyField_String_Where>;
  couleurDuBouton?: InputMaybe<IGenCaisyField_Color_Where>;
  texte?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenSectionInformation_Sort = {
  bouton?: InputMaybe<IGenOrder>;
  couleurDuBouton?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  lienDuBouton?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  sectionImage?: InputMaybe<IGenOrder>;
  texte?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenSectionInformation_Title_Where = {
  findOne?: InputMaybe<IGenSectionInformation_Title_WhereConnection>;
};

export type IGenSectionInformation_Title_WhereConnection = {
  Title?: InputMaybe<IGenTitle_Nested_Where>;
};

export type IGenSectionInformation_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenSectionInformation_Where>>>;
  bouton?: InputMaybe<IGenCaisyField_String_Where>;
  couleurDuBouton?: InputMaybe<IGenCaisyField_Color_Where>;
  lienDuBouton?: InputMaybe<IGenSectionInformation_LienDuBouton_Where>;
  texte?: InputMaybe<IGenCaisyField_String_Where>;
  title?: InputMaybe<IGenSectionInformation_Title_Where>;
};

export type IGenSeoInformation = {
  __typename?: 'SeoInformation';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  keywords?: Maybe<Scalars['String']>;
  ogImage?: Maybe<IGenAsset>;
  title?: Maybe<Scalars['String']>;
  titleInternal?: Maybe<Scalars['String']>;
};


export type IGenSeoInformationOgImageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenSeoInformation_Connection = {
  __typename?: 'SeoInformation_Connection';
  edges?: Maybe<Array<Maybe<IGenSeoInformation_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenSeoInformation_ConnectionEdge = {
  __typename?: 'SeoInformation_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenSeoInformation>;
};

export type IGenSeoInformation_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Nested_Where>>>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  keywords?: InputMaybe<IGenCaisyField_String_Where>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenSeoInformation_OgImage_Where = {
  findOne?: InputMaybe<IGenSeoInformation_OgImage_WhereConnection>;
};

export type IGenSeoInformation_OgImage_WhereConnection = {
  Asset?: InputMaybe<IGenAsset_Nested_Where>;
};

export type IGenSeoInformation_Sort = {
  createdAt?: InputMaybe<IGenOrder>;
  description?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  keywords?: InputMaybe<IGenOrder>;
  ogImage?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  titleInternal?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenSeoInformation_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenSeoInformation_Where>>>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  keywords?: InputMaybe<IGenCaisyField_String_Where>;
  ogImage?: InputMaybe<IGenSeoInformation_OgImage_Where>;
  title?: InputMaybe<IGenCaisyField_String_Where>;
  titleInternal?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenTitle = {
  __typename?: 'Title';
  FontPrimaryText?: Maybe<Scalars['String']>;
  FontSecondaryText?: Maybe<Scalars['String']>;
  PrimaryText?: Maybe<Scalars['String']>;
  SecondaryText?: Maybe<Scalars['String']>;
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  id?: Maybe<Scalars['ID']>;
  textColorOne?: Maybe<IGenCouleur>;
  textColorTwo?: Maybe<IGenCouleur>;
};


export type IGenTitleTextColorOneArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenTitleTextColorTwoArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenTitle_Connection = {
  __typename?: 'Title_Connection';
  edges?: Maybe<Array<Maybe<IGenTitle_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenTitle_ConnectionEdge = {
  __typename?: 'Title_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenTitle>;
};

export enum IGenTitle_FontPrimaryText {
  Bold = 'bold',
  Normal = 'normal'
}

export type IGenTitle_FontPrimaryText_Where = {
  eq?: InputMaybe<IGenTitle_FontPrimaryText>;
};

export enum IGenTitle_FontSecondaryText {
  Bold = 'bold',
  Normal = 'normal'
}

export type IGenTitle_FontSecondaryText_Where = {
  eq?: InputMaybe<IGenTitle_FontSecondaryText>;
};

export type IGenTitle_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenTitle_Nested_Where>>>;
  FontPrimaryText?: InputMaybe<IGenTitle_FontPrimaryText_Where>;
  FontSecondaryText?: InputMaybe<IGenTitle_FontSecondaryText_Where>;
  OR?: InputMaybe<Array<InputMaybe<IGenTitle_Nested_Where>>>;
  PrimaryText?: InputMaybe<IGenCaisyField_String_Where>;
  SecondaryText?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenTitle_Sort = {
  FontPrimaryText?: InputMaybe<IGenOrder>;
  FontSecondaryText?: InputMaybe<IGenOrder>;
  PrimaryText?: InputMaybe<IGenOrder>;
  SecondaryText?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  textColorOne?: InputMaybe<IGenOrder>;
  textColorTwo?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenTitle_TextColorOne_Where = {
  findOne?: InputMaybe<IGenTitle_TextColorOne_WhereConnection>;
};

export type IGenTitle_TextColorOne_WhereConnection = {
  Couleur?: InputMaybe<IGenCouleur_Nested_Where>;
};

export type IGenTitle_TextColorTwo_Where = {
  findOne?: InputMaybe<IGenTitle_TextColorTwo_WhereConnection>;
};

export type IGenTitle_TextColorTwo_WhereConnection = {
  Couleur?: InputMaybe<IGenCouleur_Nested_Where>;
};

export type IGenTitle_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenTitle_Where>>>;
  FontPrimaryText?: InputMaybe<IGenTitle_FontPrimaryText_Where>;
  FontSecondaryText?: InputMaybe<IGenTitle_FontSecondaryText_Where>;
  OR?: InputMaybe<Array<InputMaybe<IGenTitle_Where>>>;
  PrimaryText?: InputMaybe<IGenCaisyField_String_Where>;
  SecondaryText?: InputMaybe<IGenCaisyField_String_Where>;
  textColorOne?: InputMaybe<IGenTitle_TextColorOne_Where>;
  textColorTwo?: InputMaybe<IGenTitle_TextColorTwo_Where>;
};

export type IGenWorkerSection = {
  __typename?: 'WorkerSection';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  couleurDeFond?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  imageDeFond?: Maybe<IGenAsset>;
  slider?: Maybe<Array<Maybe<IGenWorkerSection_Slider>>>;
  title?: Maybe<IGenTitle>;
  titreSecondaire?: Maybe<Scalars['String']>;
};


export type IGenWorkerSectionImageDeFondArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenWorkerSectionSliderArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenWorkerSectionTitleArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export type IGenWorkerSection_Connection = {
  __typename?: 'WorkerSection_Connection';
  edges?: Maybe<Array<Maybe<IGenWorkerSection_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenWorkerSection_ConnectionEdge = {
  __typename?: 'WorkerSection_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenWorkerSection>;
};

export type IGenWorkerSection_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Nested_Where>>>;
  couleurDeFond?: InputMaybe<IGenCaisyField_Color_Where>;
  titreSecondaire?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenWorkerSection_Slider_Where = {
  findOne?: InputMaybe<IGenWorkerSection_Slider_WhereConnection>;
};

export type IGenWorkerSection_Slider_WhereConnection = {
  WorkerSlider?: InputMaybe<IGenWorkerSlider_Nested_Where>;
};

export type IGenWorkerSection_Sort = {
  couleurDeFond?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  imageDeFond?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  slider?: InputMaybe<IGenOrder>;
  title?: InputMaybe<IGenOrder>;
  titreSecondaire?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenWorkerSection_Title_Where = {
  findOne?: InputMaybe<IGenWorkerSection_Title_WhereConnection>;
};

export type IGenWorkerSection_Title_WhereConnection = {
  Title?: InputMaybe<IGenTitle_Nested_Where>;
};

export type IGenWorkerSection_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenWorkerSection_Where>>>;
  couleurDeFond?: InputMaybe<IGenCaisyField_Color_Where>;
  slider?: InputMaybe<IGenWorkerSection_Slider_Where>;
  title?: InputMaybe<IGenWorkerSection_Title_Where>;
  titreSecondaire?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenWorkerSection_Slider = IGenWorkerSlider;

export type IGenWorkerSlider = {
  __typename?: 'WorkerSlider';
  _meta?: Maybe<IGenCaisyDocument_Meta>;
  categorie?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  image?: Maybe<IGenAsset>;
  lienBouton?: Maybe<Array<Maybe<IGenWorkerSlider_LienBouton>>>;
  sousTitre?: Maybe<Scalars['String']>;
  texteBouton?: Maybe<Scalars['String']>;
  titre?: Maybe<Scalars['String']>;
};


export type IGenWorkerSliderImageArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};


export type IGenWorkerSliderLienBoutonArgs = {
  after?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  locale?: InputMaybe<Scalars['String']>;
};

export enum IGenWorkerSlider_Categorie {
  Employeur = 'employeur',
  Personnel = 'personnel'
}

export type IGenWorkerSlider_Categorie_Where = {
  eq?: InputMaybe<IGenWorkerSlider_Categorie>;
};

export type IGenWorkerSlider_Connection = {
  __typename?: 'WorkerSlider_Connection';
  edges?: Maybe<Array<Maybe<IGenWorkerSlider_ConnectionEdge>>>;
  pageInfo?: Maybe<IGenPageInfo>;
  totalCount?: Maybe<Scalars['Int']>;
};

export type IGenWorkerSlider_ConnectionEdge = {
  __typename?: 'WorkerSlider_ConnectionEdge';
  cursor?: Maybe<Scalars['String']>;
  node?: Maybe<IGenWorkerSlider>;
};

export type IGenWorkerSlider_Nested_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Nested_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Nested_Where>>>;
  categorie?: InputMaybe<IGenWorkerSlider_Categorie_Where>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  sousTitre?: InputMaybe<IGenCaisyField_String_Where>;
  texteBouton?: InputMaybe<IGenCaisyField_String_Where>;
  titre?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenWorkerSlider_Sort = {
  categorie?: InputMaybe<IGenOrder>;
  createdAt?: InputMaybe<IGenOrder>;
  description?: InputMaybe<IGenOrder>;
  id?: InputMaybe<IGenOrder>;
  image?: InputMaybe<IGenOrder>;
  lienBouton?: InputMaybe<IGenOrder>;
  publishedAt?: InputMaybe<IGenOrder>;
  sousTitre?: InputMaybe<IGenOrder>;
  texteBouton?: InputMaybe<IGenOrder>;
  titre?: InputMaybe<IGenOrder>;
  updatedAt?: InputMaybe<IGenOrder>;
};

export type IGenWorkerSlider_Where = {
  AND?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Where>>>;
  OR?: InputMaybe<Array<InputMaybe<IGenWorkerSlider_Where>>>;
  categorie?: InputMaybe<IGenWorkerSlider_Categorie_Where>;
  description?: InputMaybe<IGenCaisyField_String_Where>;
  sousTitre?: InputMaybe<IGenCaisyField_String_Where>;
  texteBouton?: InputMaybe<IGenCaisyField_String_Where>;
  titre?: InputMaybe<IGenCaisyField_String_Where>;
};

export type IGenWorkerSlider_LienBouton = IGenBlogArticle | IGenCategorieOffresDemploi | IGenCategory | IGenCouleur | IGenNavigation | IGenNavigationEntry | IGenOffresEmploi | IGenPage | IGenSeoInformation;

export type IGenAssetFragment = { __typename?: 'Asset', title?: string | null, src?: string | null, originType?: string | null, keywords?: string | null, id?: string | null, dominantColor?: string | null, description?: string | null, copyright?: string | null };

export type IGenBlogArticleFragment = { __typename?: 'BlogArticle', teaserHeadline?: string | null, teaserDesciption?: string | null, slug?: string | null, id?: string | null, text?: { __typename?: 'BlogArticle_text', json?: any | null, connections?: Array<(
      { __typename: 'Asset' }
      & IGenAssetFragment
    ) | null> | null } | null, teaserImage?: (
    { __typename?: 'Asset' }
    & IGenAssetFragment
  ) | null, seo?: { __typename?: 'SeoInformation', id?: string | null, description?: string | null, keywords?: string | null, title?: string | null, ogImage?: (
      { __typename?: 'Asset' }
      & IGenAssetFragment
    ) | null } | null };

export type IGenBlogArticleGridFragment = { __typename?: 'BlogArticleGrid', id?: string | null, headline?: string | null, subheadline?: string | null, articles?: Array<{ __typename?: 'BlogArticle', id?: string | null, slug?: string | null, teaserDesciption?: string | null, teaserHeadline?: string | null, teaserImage?: (
      { __typename?: 'Asset' }
      & IGenAssetFragment
    ) | null, text?: { __typename?: 'BlogArticle_text', json?: any | null, connections?: Array<(
        { __typename: 'Asset' }
        & IGenAssetFragment
      ) | null> | null } | null } | null> | null };

export type IGenCategoryFragment = { __typename?: 'Category', name?: string | null, id?: string | null };

export type IGenContactFormFragment = { __typename?: 'ContactForm', id?: string | null, headline?: string | null };

export type IGenFulltextFragment = { __typename?: 'Fulltext', id?: string | null, text?: { __typename?: 'Fulltext_text', json?: any | null, connections?: Array<(
      { __typename: 'Asset' }
      & IGenAssetFragment
    ) | null> | null } | null };

export type IGenFullwidthBlogTeaserFragment = { __typename?: 'FullwidthBlogTeaser', id?: string | null, featuredArticle?: { __typename?: 'BlogArticle', id?: string | null, slug?: string | null, teaserHeadline?: string | null, teaserDesciption?: string | null, teaserImage?: (
      { __typename?: 'Asset' }
      & IGenAssetFragment
    ) | null, text?: { __typename?: 'BlogArticle_text', json?: any | null, connections?: Array<(
        { __typename: 'Asset' }
        & IGenAssetFragment
      ) | null> | null } | null, category?: (
      { __typename?: 'Category' }
      & IGenCategoryFragment
    ) | null } | null };

export type IGenNavigationEntryFragment = { __typename?: 'NavigationEntry', id?: string | null, title?: string | null, connection?: { __typename?: 'Page', id?: string | null, slug?: string | null } | null };

export type IGenNewsletterSignupFragment = { __typename?: 'NewsletterSignup', headline?: string | null, subheadline?: string | null, id?: string | null };

export type IGenOffresEmploiGridFragment = { __typename?: 'OffresEmploiGrid', id?: string | null, description?: string | null, offresDemploi?: Array<{ __typename?: 'OffresEmploi', id?: string | null, lieuDeLoffre?: string | null, resumerDeLoffre?: string | null, slug?: string | null, titreDeLoffre?: string | null, typeDoffre?: string | null, categorie?: Array<{ __typename?: 'CategorieOffresDemploi', id?: string | null, name?: string | null } | null> | null, text?: { __typename?: 'OffresEmploi_text', json?: any | null } | null } | null> | null, image?: (
    { __typename?: 'Asset' }
    & IGenAssetFragment
  ) | null, title?: (
    { __typename?: 'Title' }
    & IGenTitleFragment
  ) | null };

export type IGenPageFragment = { __typename?: 'Page', id?: string | null, slug?: string | null, components?: Array<(
    { __typename: 'BlogArticleGrid' }
    & IGenBlogArticleGridFragment
  ) | (
    { __typename: 'ContactForm' }
    & IGenContactFormFragment
  ) | (
    { __typename: 'Fulltext' }
    & IGenFulltextFragment
  ) | (
    { __typename: 'FullwidthBlogTeaser' }
    & IGenFullwidthBlogTeaserFragment
  ) | { __typename: 'LogoSection' } | { __typename: 'LogoSlider' } | (
    { __typename: 'NewsletterSignup' }
    & IGenNewsletterSignupFragment
  ) | (
    { __typename: 'OffresEmploiGrid' }
    & IGenOffresEmploiGridFragment
  ) | (
    { __typename: 'SectionInformation' }
    & IGenSectionInformationFragment
  ) | (
    { __typename: 'Title' }
    & IGenTitleFragment
  ) | (
    { __typename: 'WorkerSection' }
    & IGenWorkerSectionFragment
  ) | (
    { __typename: 'WorkerSlider' }
    & IGenWorkerSliderFragment
  ) | null> | null, seo?: { __typename?: 'SeoInformation', title?: string | null, keywords?: string | null, id?: string | null, description?: string | null, ogImage?: { __typename?: 'Asset', id?: string | null, src?: string | null, description?: string | null } | null } | null };

export type IGenSectionInformationFragment = { __typename?: 'SectionInformation', texte?: string | null, bouton?: string | null, id?: string | null, title?: (
    { __typename?: 'Title' }
    & IGenTitleFragment
  ) | null, lienDuBouton?: (
    { __typename?: 'NavigationEntry' }
    & IGenNavigationEntryFragment
  ) | null, sectionImage?: (
    { __typename?: 'Asset' }
    & IGenAssetFragment
  ) | null };

export type IGenTitleFragment = { __typename?: 'Title', PrimaryText?: string | null, SecondaryText?: string | null, FontPrimaryText?: string | null, FontSecondaryText?: string | null, id?: string | null, textColorOne?: { __typename?: 'Couleur', couleur?: string | null } | null, textColorTwo?: { __typename?: 'Couleur', couleur?: string | null } | null };

export type IGenWorkerSectionFragment = { __typename?: 'WorkerSection', couleurDeFond?: string | null, titreSecondaire?: string | null, id?: string | null, title?: (
    { __typename?: 'Title' }
    & IGenTitleFragment
  ) | null, imageDeFond?: (
    { __typename?: 'Asset' }
    & IGenAssetFragment
  ) | null, slider?: Array<{ __typename?: 'WorkerSlider', categorie?: string | null, titre?: string | null, sousTitre?: string | null, description?: string | null, texteBouton?: string | null, id?: string | null, lienBouton?: Array<{ __typename?: 'BlogArticle', slug?: string | null } | { __typename?: 'CategorieOffresDemploi' } | { __typename?: 'Category' } | { __typename?: 'Couleur' } | { __typename?: 'Navigation' } | { __typename?: 'NavigationEntry' } | { __typename?: 'OffresEmploi' } | { __typename?: 'Page', slug?: string | null } | { __typename?: 'SeoInformation' } | null> | null, image?: (
      { __typename?: 'Asset' }
      & IGenAssetFragment
    ) | null } | null> | null };

export type IGenWorkerSliderFragment = { __typename?: 'WorkerSlider', categorie?: string | null, titre?: string | null, sousTitre?: string | null, description?: string | null, texteBouton?: string | null, id?: string | null, lienBouton?: Array<{ __typename?: 'BlogArticle', slug?: string | null } | { __typename?: 'CategorieOffresDemploi' } | { __typename?: 'Category' } | { __typename?: 'Couleur' } | { __typename?: 'Navigation' } | { __typename?: 'NavigationEntry' } | { __typename?: 'OffresEmploi' } | { __typename?: 'Page', slug?: string | null } | { __typename?: 'SeoInformation' } | null> | null, image?: (
    { __typename?: 'Asset' }
    & IGenAssetFragment
  ) | null };

export type IGenNavigationQueryVariables = Exact<{ [key: string]: never; }>;


export type IGenNavigationQuery = { __typename?: 'Query', Navigation?: { __typename?: 'Navigation', id?: string | null, homePage?: { __typename?: 'Page', id?: string | null, slug?: string | null } | null, notFoundPage?: { __typename?: 'Page', id?: string | null, slug?: string | null } | null, entries?: Array<(
      { __typename?: 'NavigationEntry' }
      & IGenNavigationEntryFragment
    ) | null> | null } | null };

export type IGenAllBlogArticleBySlugQueryVariables = Exact<{
  slug: Scalars['String'];
}>;


export type IGenAllBlogArticleBySlugQuery = { __typename?: 'Query', allBlogArticle?: { __typename?: 'BlogArticle_Connection', edges?: Array<{ __typename?: 'BlogArticle_ConnectionEdge', node?: (
        { __typename?: 'BlogArticle' }
        & IGenBlogArticleFragment
      ) | null } | null> | null } | null };

export type IGenAllBlogArticleMetaQueryVariables = Exact<{
  after?: InputMaybe<Scalars['String']>;
}>;


export type IGenAllBlogArticleMetaQuery = { __typename?: 'Query', allBlogArticle?: { __typename?: 'BlogArticle_Connection', totalCount?: number | null, pageInfo?: { __typename?: 'PageInfo', hasNextPage?: boolean | null, endCursor?: string | null } | null, edges?: Array<{ __typename?: 'BlogArticle_ConnectionEdge', node?: { __typename?: 'BlogArticle', id?: string | null, slug?: string | null, _meta?: { __typename?: 'CaisyDocument_Meta', publishedAt?: any | null } | null } | null } | null> | null } | null };

export type IGenAllPageBySlugQueryVariables = Exact<{
  slug: Scalars['String'];
}>;


export type IGenAllPageBySlugQuery = { __typename?: 'Query', allPage?: { __typename?: 'Page_Connection', edges?: Array<{ __typename?: 'Page_ConnectionEdge', node?: (
        { __typename?: 'Page' }
        & IGenPageFragment
      ) | null } | null> | null } | null };

export type IGenAllPageMetaQueryVariables = Exact<{
  after?: InputMaybe<Scalars['String']>;
}>;


export type IGenAllPageMetaQuery = { __typename?: 'Query', allPage?: { __typename?: 'Page_Connection', totalCount?: number | null, pageInfo?: { __typename?: 'PageInfo', hasNextPage?: boolean | null, endCursor?: string | null } | null, edges?: Array<{ __typename?: 'Page_ConnectionEdge', node?: { __typename?: 'Page', id?: string | null, slug?: string | null, _meta?: { __typename?: 'CaisyDocument_Meta', publishedAt?: any | null } | null } | null } | null> | null } | null };

export const AssetFragmentDoc = gql`
    fragment Asset on Asset {
  title
  src
  originType
  keywords
  id
  dominantColor
  description
  copyright
}
    `;
export const BlogArticleFragmentDoc = gql`
    fragment BlogArticle on BlogArticle {
  text {
    connections {
      __typename
      ...Asset
    }
    json
  }
  teaserImage {
    ...Asset
  }
  teaserHeadline
  teaserDesciption
  slug
  seo {
    id
    description
    keywords
    ogImage {
      ...Asset
    }
    title
  }
  id
}
    `;
export const ContactFormFragmentDoc = gql`
    fragment ContactForm on ContactForm {
  id
  headline
}
    `;
export const BlogArticleGridFragmentDoc = gql`
    fragment BlogArticleGrid on BlogArticleGrid {
  id
  headline
  subheadline
  articles {
    ... on BlogArticle {
      id
      slug
      teaserDesciption
      teaserHeadline
      teaserImage {
        ...Asset
      }
      text {
        connections {
          __typename
          ...Asset
        }
        json
      }
    }
  }
}
    `;
export const NewsletterSignupFragmentDoc = gql`
    fragment NewsletterSignup on NewsletterSignup {
  headline
  subheadline
  id
}
    `;
export const CategoryFragmentDoc = gql`
    fragment Category on Category {
  name
  id
}
    `;
export const FullwidthBlogTeaserFragmentDoc = gql`
    fragment FullwidthBlogTeaser on FullwidthBlogTeaser {
  id
  featuredArticle {
    id
    slug
    teaserImage {
      ...Asset
    }
    text {
      connections {
        __typename
        ... on Asset {
          ...Asset
        }
      }
      json
    }
    category {
      ...Category
    }
    teaserHeadline
    teaserDesciption
  }
  id
}
    `;
export const FulltextFragmentDoc = gql`
    fragment Fulltext on Fulltext {
  text {
    json
    connections {
      __typename
      ...Asset
    }
  }
  id
}
    `;
export const TitleFragmentDoc = gql`
    fragment Title on Title {
  PrimaryText
  SecondaryText
  FontPrimaryText
  FontSecondaryText
  textColorOne {
    couleur
  }
  textColorTwo {
    couleur
  }
  id
}
    `;
export const NavigationEntryFragmentDoc = gql`
    fragment NavigationEntry on NavigationEntry {
  id
  title
  connection {
    id
    slug
  }
}
    `;
export const SectionInformationFragmentDoc = gql`
    fragment SectionInformation on SectionInformation {
  texte
  title {
    ...Title
  }
  bouton
  lienDuBouton {
    ...NavigationEntry
  }
  sectionImage {
    ...Asset
  }
  id
}
    `;
export const WorkerSectionFragmentDoc = gql`
    fragment WorkerSection on WorkerSection {
  title {
    ...Title
  }
  imageDeFond {
    ...Asset
  }
  couleurDeFond
  titreSecondaire
  slider {
    ... on WorkerSlider {
      categorie
      titre
      sousTitre
      description
      texteBouton
      lienBouton {
        ... on Page {
          slug
        }
        ... on BlogArticle {
          slug
        }
      }
      image {
        ...Asset
      }
      id
    }
  }
  id
}
    `;
export const WorkerSliderFragmentDoc = gql`
    fragment WorkerSlider on WorkerSlider {
  categorie
  titre
  sousTitre
  description
  texteBouton
  lienBouton {
    ... on Page {
      slug
    }
    ... on BlogArticle {
      slug
    }
  }
  image {
    ...Asset
  }
  id
}
    `;
export const OffresEmploiGridFragmentDoc = gql`
    fragment OffresEmploiGrid on OffresEmploiGrid {
  id
  description
  offresDemploi {
    ... on OffresEmploi {
      id
      lieuDeLoffre
      resumerDeLoffre
      categorie {
        ... on CategorieOffresDemploi {
          id
          name
        }
      }
      slug
      titreDeLoffre
      typeDoffre
      text {
        json
      }
    }
  }
  image {
    ...Asset
  }
  title {
    ...Title
  }
}
    `;
export const PageFragmentDoc = gql`
    fragment Page on Page {
  components {
    __typename
    ...ContactForm
    ...BlogArticleGrid
    ...NewsletterSignup
    ...FullwidthBlogTeaser
    ...Fulltext
    ...SectionInformation
    ...WorkerSection
    ...WorkerSlider
    ...Title
    ...OffresEmploiGrid
  }
  id
  seo {
    title
    ogImage {
      id
      src
      description
    }
    keywords
    id
    description
  }
  slug
}
    `;
export const NavigationDocument = gql`
    query Navigation {
  Navigation {
    id
    homePage {
      id
      slug
    }
    notFoundPage {
      id
      slug
    }
    entries {
      ...NavigationEntry
    }
  }
}
    ${NavigationEntryFragmentDoc}`;
export const AllBlogArticleBySlugDocument = gql`
    query allBlogArticleBySlug($slug: String!) {
  allBlogArticle(where: {slug: {eq: $slug}}) {
    edges {
      node {
        ...BlogArticle
      }
    }
  }
}
    ${BlogArticleFragmentDoc}
${AssetFragmentDoc}`;
export const AllBlogArticleMetaDocument = gql`
    query allBlogArticleMeta($after: String) {
  allBlogArticle(after: $after) {
    totalCount
    pageInfo {
      hasNextPage
      endCursor
    }
    edges {
      node {
        _meta {
          publishedAt
        }
        id
        slug
      }
    }
  }
}
    `;
export const AllPageBySlugDocument = gql`
    query allPageBySlug($slug: String!) {
  allPage(where: {slug: {eq: $slug}}) {
    edges {
      node {
        ...Page
      }
    }
  }
}
    ${PageFragmentDoc}
${ContactFormFragmentDoc}
${BlogArticleGridFragmentDoc}
${AssetFragmentDoc}
${NewsletterSignupFragmentDoc}
${FullwidthBlogTeaserFragmentDoc}
${CategoryFragmentDoc}
${FulltextFragmentDoc}
${SectionInformationFragmentDoc}
${TitleFragmentDoc}
${NavigationEntryFragmentDoc}
${WorkerSectionFragmentDoc}
${WorkerSliderFragmentDoc}
${OffresEmploiGridFragmentDoc}`;
export const AllPageMetaDocument = gql`
    query allPageMeta($after: String) {
  allPage(after: $after) {
    totalCount
    pageInfo {
      hasNextPage
      endCursor
    }
    edges {
      node {
        _meta {
          publishedAt
        }
        id
        slug
      }
    }
  }
}
    `;
export type Requester<C = {}, E = unknown> = <R, V>(doc: DocumentNode, vars?: V, options?: C) => Promise<R> | AsyncIterable<R>
export function getSdk<C, E>(requester: Requester<C, E>) {
  return {
    Navigation(variables?: IGenNavigationQueryVariables, options?: C): Promise<IGenNavigationQuery> {
      return requester<IGenNavigationQuery, IGenNavigationQueryVariables>(NavigationDocument, variables, options) as Promise<IGenNavigationQuery>;
    },
    allBlogArticleBySlug(variables: IGenAllBlogArticleBySlugQueryVariables, options?: C): Promise<IGenAllBlogArticleBySlugQuery> {
      return requester<IGenAllBlogArticleBySlugQuery, IGenAllBlogArticleBySlugQueryVariables>(AllBlogArticleBySlugDocument, variables, options) as Promise<IGenAllBlogArticleBySlugQuery>;
    },
    allBlogArticleMeta(variables?: IGenAllBlogArticleMetaQueryVariables, options?: C): Promise<IGenAllBlogArticleMetaQuery> {
      return requester<IGenAllBlogArticleMetaQuery, IGenAllBlogArticleMetaQueryVariables>(AllBlogArticleMetaDocument, variables, options) as Promise<IGenAllBlogArticleMetaQuery>;
    },
    allPageBySlug(variables: IGenAllPageBySlugQueryVariables, options?: C): Promise<IGenAllPageBySlugQuery> {
      return requester<IGenAllPageBySlugQuery, IGenAllPageBySlugQueryVariables>(AllPageBySlugDocument, variables, options) as Promise<IGenAllPageBySlugQuery>;
    },
    allPageMeta(variables?: IGenAllPageMetaQueryVariables, options?: C): Promise<IGenAllPageMetaQuery> {
      return requester<IGenAllPageMetaQuery, IGenAllPageMetaQueryVariables>(AllPageMetaDocument, variables, options) as Promise<IGenAllPageMetaQuery>;
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;