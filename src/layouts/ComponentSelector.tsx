import React from "react";
import { BlogArticleGrid } from "../components/blog-article-grid/BlogArticleGrid";
import { ContactForm } from "../components/ContactForm";
import { FullText } from "../components/fulltext/FullText";
import { FullwidthBlogTeaser } from "../components/FullwidthBlogTeaser";
// import { Headline } from "../components/Headline";
import { NewsletterSignup } from "../components/NewsletterSignup";
import { SectionInformation } from "../components/section/SectionInformation";
import { WorkerSection } from "../components/section/WorkerSection";
import { WorkerSlider } from "../components/section/WorkerSlider";
import { Title } from "../components/Title";
import { OffresEmploiGrid } from "../components/offre-emploi-grid/OffreEmploiGrid";
import { IGenPage_Components } from "../services/graphql/__generated/sdk";
interface IComponentSelector {
  component: IGenPage_Components;
}

export const ComponentSelector: React.FC<IComponentSelector> = ({
  component,
}) => {
  return (
    <>
      {/* {component.__typename == "Headline" && <Headline {...component} />} */}
      {component.__typename == "Fulltext" && <FullText {...component} />}
      {component.__typename == "BlogArticleGrid" && (
        <BlogArticleGrid {...component} />
      )}
      {component.__typename == "NewsletterSignup" && (
        <NewsletterSignup {...component} />
      )}
      {component.__typename == "FullwidthBlogTeaser" && (
        <FullwidthBlogTeaser {...component} />
      )}
      {component.__typename == "ContactForm" && <ContactForm {...component} />}
      {component.__typename == "SectionInformation" && (
        <SectionInformation {...component} />
      )}
      {component.__typename == "WorkerSection" && (
        <WorkerSection {...component} />
      )}
      {component.__typename == "WorkerSlider" && (
        <WorkerSlider {...component} />
      )}
      {component.__typename == "Title" && <Title {...component} />}
      {component.__typename == "OffresEmploiGrid" && (
        <OffresEmploiGrid {...component} />
      )}
    </>
  );
};
