import React from "react";
import { IGenSectionInformation } from "../../services/graphql/__generated/sdk";
import { CenterContainer } from "../CenterContainer";
import { Title } from "../Title";
import Link from "next/link";

export const SectionInformation: React.FC<IGenSectionInformation> = ({
  title,
  texte,
  sectionImage,
  bouton,
  lienDuBouton,
}) => {
  const sectionImageSrc = sectionImage?.src;
  const sectionImageDescription = sectionImage?.description;
  const sectionImageWidth = sectionImage?.width;
  const sectionImageHeight = sectionImage?.height;
  const linkButton = lienDuBouton?.connection?.slug ?? "";
  return (
    <CenterContainer>
      <div className="mb-8 flex flex-col justify-start items-center gap-6">
        <div className="flex w-full justify-between items-start">
          {title && (
            <h2 className="text-[3.373vw] [&>:first-child]:text-[2.315vw] flex flex-col">
              <Title {...title} />
            </h2>
          )}
          {texte && (
            <p className="w-[38.889vw] text-[1.190vw] text-left text-gray-400 leading-[175%] font-normal">
              {texte}
            </p>
          )}
        </div>
        {sectionImage?.src && (
          <div className="relative">
            <div
              className="w-full h-[29.365vw] overflow-hidden flex items-center justify-center rounded-[1.852vw]"
              style={{
                clipPath:
                  "polygon(100% 0%, 70% 0%, 35% 0%, 0% 0%, 0% 25%, 0% 47%, 0% 100%, 24% 100%, 40% 100%, 75% 100%, 75% 75%, 100% 75%, 100% 45%, 100% 25%)",
              }}
            >
              <img
                loading="eager"
                src={`${sectionImageSrc}?w=${sectionImageWidth}&h=${sectionImageHeight}`}
                srcSet={`${sectionImageSrc}?w=1920&h=1920 1920w, ${sectionImageSrc}?w=960&h=960 1280w, ${sectionImageSrc}?w=640&h=640 640w, ${sectionImageSrc}?w=320&h=320 320w`}
                alt={sectionImageDescription ?? ""}
                className="object-cover"
                width={`${sectionImageWidth}`}
                height={`${sectionImageHeight}`}
              />
            </div>
            {}
            <Link
              href={`/${linkButton}`}
              className="z-10 absolute pointer w-[25.331vw] h-[6.878vw] bg-primary flex justify-center items-center rounded-[1.852vw] bottom-2 right-2 outline outline-offset-0 outline-[14px] outline-quaternary"
            >
              {bouton}
            </Link>
          </div>
        )}
      </div>
    </CenterContainer>
  );
};
