import React from "react";
import { IGenWorkerSlider } from "../../services/graphql/__generated/sdk";

export const WorkerSlider: React.FC<IGenWorkerSlider> = ({
  titre,
  sousTitre,
  description,
  texteBouton,
  image,
}) => {
  return (
    <div className="w-full">
      <div className="flex flex-row-reverse justify-start items-center gap-[56px]">
        <div className="space-y-[21px]">
          {titre && (
            <h2 className="text-[60px] font-black text-left text-secondary">
              {titre}
            </h2>
          )}
          {sousTitre && (
            <p className="text-[17px] font-medium text-left text-primary">
              {sousTitre}
            </p>
          )}
          {description && (
            <p className="text-[16px] text-left text-tertiary">{description}</p>
          )}
          {texteBouton && (
            <span className="text-sm text-left text-slate-900">
              {texteBouton}
            </span>
          )}
        </div>
        {image?.src && (
          <img
            loading="eager"
            src={`${image.src}?w=${image.width}&h=${image.height}`}
            srcSet={`${image.src}?w=1920&h=1920 1920w, ${image.src}?w=960&h=960 1280w, ${image.src}?w=640&h=640 640w, ${image.src}?w=320&h=320 320w`}
            alt={image.description ?? ""}
            className="object-cover rounded-tr-[138px] rounded-bl-[138px]"
            width={"559"}
            height={"500"}
          />
        )}
      </div>
    </div>
  );
};
