"use client";
import React, { useState, useRef, useMemo } from "react";
import { IGenWorkerSection } from "../../services/graphql/__generated/sdk";
import { WorkerSlider } from "./WorkerSlider";
import Carousel from "framer-motion-carousel";
import { Title } from "../Title";

export const WorkerSection: React.FC<IGenWorkerSection> = ({
  title,
  titreSecondaire,
  slider,
  couleurDeFond,
}) => {
  const [selectedCategory, setSelectedCategory] = useState<string>("personnel");
  const [activeSlideIndex, setActiveSlideIndex] = useState<number>(0);
  const carouselRef = useRef<any>(null);

  const categoriesSet = new Set<string>();
  const categories = slider?.map((categorie, index) => {
    const categoryName = categorie?.categorie;
    if (categoryName && !categoriesSet.has(categoryName)) {
      categoriesSet.add(categoryName);
      return <span key={index}>{categoryName}</span>;
    }
    return null;
  });

  const filteredSlider = useMemo(
    () =>
      slider?.filter((slide) => slide?.categorie === selectedCategory) || [],
    [slider, selectedCategory]
  );

  const categoriesArray = Array.from(categoriesSet);

  const goToSlide = (index) => {
    setActiveSlideIndex(index); // Met à jour l'index actif de la slide
    carouselRef.current?.setIndex(index); // Navigue vers la slide spécifique
  };

  return (
    <div className="w-full px-[51px]">
      <div className="flex flex-col justify-start items-start gap-[37px]">
        {title && (
          <h2 className="text-[3.373vw] [&>:first-child]:text-[2.315vw] flex flex-col">
            <Title {...title} />
          </h2>
        )}
        <div
          className="w-full flex flex-col gap-[28px] justify-center items-center p-[28px] rounded-t-[28px] rounded-br-[28px] rounded-bl-[158px] overflow-hidden"
          style={{ backgroundColor: couleurDeFond ? couleurDeFond : "" }}
        >
          {titreSecondaire && (
            <h4 className="text-[21px] text-primary">{titreSecondaire}</h4>
          )}
          <div className="flex text-[18px] rounded-full border-2 border-primary">
            {categoriesArray.map((category, index) => (
              <p
                key={index}
                className={`px-[38px] py-[16px] rounded-full ${
                  category === selectedCategory
                    ? "bg-primary text-white"
                    : "bg-transparent text-primary hover:text-white hover:bg-black hover:cursor-pointer"
                }`}
                onClick={() => {
                  setSelectedCategory(category);
                  carouselRef.current?.setIndex(0);
                  setActiveSlideIndex(0);
                }}
              >
                {category === "employeur" ? "Employeur" : "Personnel de santé"}
              </p>
            ))}
          </div>
          <div className="flex gap-3">
            {filteredSlider.map((slideTitle, index) => (
              <div
                key={index}
                className={`flex items-center gap-3 text-[22px] ${
                  activeSlideIndex === index ? "text-tertiary" : "text-primary"
                }`}
              >
                <div
                  className={`h-2.5 w-2.5 rounded-full ${
                    activeSlideIndex === index ? "bg-tertiary" : "bg-primary"
                  }`}
                ></div>
                <button onClick={() => goToSlide(index)}>
                  {slideTitle?.titre}
                </button>
              </div>
            ))}
          </div>

          <Carousel
            autoPlay={false}
            interval={7500}
            loop={false}
            ref={carouselRef}
            renderArrowLeft={() => <button className="hidden"></button>}
            renderArrowRight={() => <button className="hidden"></button>}
            renderDots={() => <div className="hidden"></div>}
          >
            {slider?.map((slide, index) => {
              if (slide?.categorie === selectedCategory) {
                return <WorkerSlider {...slide} key={index} />;
              }
              return null;
            })}
          </Carousel>
        </div>
      </div>
    </div>
  );
};
