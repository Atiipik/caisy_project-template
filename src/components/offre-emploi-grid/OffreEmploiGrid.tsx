import React from "react";
import { IGenOffresEmploiGrid } from "../../services/graphql/__generated/sdk";
import { FullContainer } from "../FullContainer";
import { OffreCard } from "./OffreCard";
import { Title } from "../Title";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import { TypographyP } from "@/components/ui/typography/TypographyP";

export const OffresEmploiGrid: React.FC<IGenOffresEmploiGrid> = ({
  title,
  description,
  image,
  offresDemploi,
}) => {
  return (
    <FullContainer>
      <Accordion type="single" collapsible className="mb-10">
        <AccordionItem value="item-1">
          <AccordionTrigger>Que dit le code du travail ?</AccordionTrigger>
          <AccordionContent>
            Le rôle du médecin du travail consiste à éviter toute altération de
            la santé des travailleurs du fait de leur travail, notamment en
            surveillant leurs conditions d’hygiène au travail, les risques de
            contagion et leur état de santé, ainsi que tout risque manifeste
            d’atteinte à la sécurité des tiers évoluant dans l’environnement
            immédiat de travail (article L. 4622-3 du Code du travail).
          </AccordionContent>
        </AccordionItem>
      </Accordion>
      <div className="relative flex items-start justify-between h-fit">
        <div className="sticky h-fit flex flex-col w-[38.228vw] top-10">
          {title && (
            <h2 className="text-[3.373vw] [&>:first-child]:text-[2.315vw] flex flex-col">
              <Title {...title} />
            </h2>
          )}
          {description && (
            <p className="w-[38.889vw] text-[1.058vw] text-left text-gray-400 leading-[175%] font-normal mt-[2.447vw] mb-[4.431vw]">
              {description}
            </p>
          )}
          {image?.src && (
            <img
              loading="eager"
              src={`${image.src}?w=${image.width}&h=${image.height}`}
              srcSet={`${image.src}?w=1920&h=1920 1920w, ${image.src}?w=960&h=960 1280w, ${image.src}?w=640&h=640 640w, ${image.src}?w=320&h=320 320w`}
              alt={image.description ?? ""}
              className="object-cover rounded-[1.852vw]"
              width={"100%"}
              height={"100%"}
            />
          )}
        </div>
        <div className="flex flex-col gap-[2.447vw]">
          {offresDemploi?.map((offre, index) => (
            <OffreCard {...offre} key={index} />
          ))}
        </div>
      </div>
      <TypographyP textSize="P14">Texte avec la taille P14</TypographyP>
      <TypographyP textSize="P16">Texte avec la taille P16</TypographyP>
      <TypographyP textSize="P18">Texte avec la taille P18</TypographyP>
      <TypographyP textSize="P20">Texte avec la taille P20</TypographyP>
    </FullContainer>
  );
};
