import React from "react";
import { IGenOffresEmploi } from "../../services/graphql/__generated/sdk";
import Link from "next/link";
import { HiOutlineMap, HiOutlineClock } from "react-icons/hi";
import { buttonVariants } from "@/components/ui/button";
import { Button } from "@/components/ui/button";
import { FiArrowRight } from "react-icons/fi";

export const OffreCard: React.FC<IGenOffresEmploi> = ({
  titreDeLoffre,
  lieuDeLoffre,
  resumerDeLoffre,
  categorie,
  slug,
  typeDoffre,
  text,
}) => {
  return (
    <div className="flex flex-col items-start justify-between gap-7 w-[46.032vw] border border-border rounded-[1.587vw] p-[1.389vw] bg-quaternary">
      <div className="flex items-baseline justify-between w-full gap-2">
        <h3 className="text-[1.852vw] font-bold text-secondary">
          {titreDeLoffre}
        </h3>
        {categorie?.map((category, index) => (
          <p key={index} className="text-[1.058vw] text-primary">
            {category?.name}
          </p>
        ))}
      </div>
      <div className="w-full h-[1px] bg-secondary"></div>
      <p className="text-[1.389vw] text-quinary line-clamp-2">
        {resumerDeLoffre}
      </p>

      <div className="flex items-center gap-2 leading-[135%] text-[1.25vw] text-quinary font-thin">
        <div className="flex gap-1.5 items-center">
          <HiOutlineMap />
          <p>{lieuDeLoffre}</p>
        </div>
        <p>-</p>
        <div className="flex gap-1.5 items-center">
          <HiOutlineClock />
          <p>{typeDoffre}</p>
        </div>
      </div>
      <div className="flex justify-end w-full">
        <Link
          href={`/blog/${slug}`}
          className={buttonVariants({ variant: "icon", size: "icon" })}
        >
          <FiArrowRight />
        </Link>
        {/* <Button>
          <BsFillArrowRightCircleFill className="w-[1.190vw] h-[1.190vw] mr-3" />
        </Button> */}
      </div>
    </div>
  );
};
