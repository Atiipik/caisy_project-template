import React from "react";

interface ICenterContainer {
  children?: React.ReactNode;
}

export const CenterContainer: React.FC<ICenterContainer> = ({ children }) => {
  return <div className="p-[3.373vw]">{children}</div>;
};
