import React from "react";

interface IFullContainer {
  children?: React.ReactNode;
}

export const FullContainer: React.FC<IFullContainer> = ({ children }) => {
  return <div className="p-[3.373vw]">{children}</div>;
};
