import React from "react";

// Définition d'un type pour les props du composant
interface TypographyPProps {
  children: React.ReactNode;
  textSize: "P14" | "P16" | "P18" | "P20"; // Définir les tailles de texte possibles en tant que variantes
}

// Composant générique pour la typographie
const TypographyP: React.FC<TypographyPProps> = ({ children, textSize }) => {
  // Définir un objet pour mapper les tailles de texte aux classes CSS
  const textSizeClasses = {
    P14: "text-[0.729vw]",
    P16: "text-[0.833vw]",
    P18: "text-[0.938vw]",
    P20: "text-[1.042vw]",
    // P14: "text-sm",
    // P16: "text-base",
    // P18: "text-lg",
    // P20: "text-xl",
  };

  // Récupérer la classe correspondant à la taille de texte demandée
  const textSizeClass = textSizeClasses[textSize];

  return <p className={`leading-[150%] ${textSizeClass}`}>{children}</p>;
};

export { TypographyP };
