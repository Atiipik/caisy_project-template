import React from "react";
import { IGenTitle } from "../services/graphql/__generated/sdk";
import classnames from "classnames";

export const Title: React.FC<IGenTitle> = ({
  PrimaryText,
  FontPrimaryText,
  SecondaryText,
  FontSecondaryText,
  textColorOne,
  textColorTwo,
}) => {
  return (
    <>
      {PrimaryText && (
        <span
          className={classnames("leading-[110%]", `font-${FontPrimaryText}`)}
          style={{ color: textColorOne?.couleur ?? "" }}
        >
          {PrimaryText}
        </span>
      )}
      {SecondaryText && (
        <span
          className={classnames("leading-[110%]", `font-${FontSecondaryText}`)}
          style={{ color: textColorTwo?.couleur ?? "" }}
        >
          {SecondaryText}
        </span>
      )}
    </>
  );
};
